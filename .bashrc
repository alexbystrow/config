[ -z "$PS1" ] && return

unset SSH_ASKPASS

# bash options ------------------------------------
shopt -s autocd             # change to named directory
shopt -s cdable_vars        # if cd arg is not valid, assumes its a var defining a dir
shopt -s cdspell            # autocorrects cd misspellings
shopt -s checkwinsize       # update the value of LINES and COLUMNS after each command if altered
shopt -s cmdhist            # save multi-line commands in history as single line
shopt -s dotglob            # include dotfiles in pathname expansion
shopt -s expand_aliases     # expand aliases
shopt -s extglob            # enable extended pattern-matching features
shopt -s histappend         # append to (not overwrite) the history file
shopt -s hostcomplete       # attempt hostname expansion when @ is at the beginning of a word
shopt -s nocaseglob         # pathname expansion will be treated as case-insensitive
shopt -s checkjobs			 # bash lists the status of any stopped and running jobs before exiting an interactive shell.

##
# exports
##
export PATH=$PATH:$HOME/bin:$HOME/bin/docker:/home/manuel/.gem/ruby/2.6.0/bin
export SUDO_PROMPT="ENTER NUCLEAR LAUNCH CODE:"
export EDITOR="vim"
export FCEDIT="vim"
export HISTCONTROL="ignoredups"
export HISTSIZE=8192
export HISTFILESIZE=8192
export LESS="-R"
export CLASSPATH=$CLASSPATH:.
export GREP_COLOR="1;33"
export COLORFGBG="default;default"

# Manpage colors
export LESS_TERMCAP_mb=$(printf "\e[1;37m")
export LESS_TERMCAP_md=$(printf "\e[1;34m")
export LESS_TERMCAP_me=$(printf "\e[0m")
export LESS_TERMCAP_se=$(printf "\e[0m")
export LESS_TERMCAP_so=$(printf "\e[46m")
export LESS_TERMCAP_ue=$(printf "\e[0m")
export LESS_TERMCAP_us=$(printf "\e[46m")

# colors 
export txtblk="\[\033[0;30m\]" # Black - Regular
export txtred="\[\033[0;31m\]" # Red
export txtgrn="\[\033[0;32m\]" # Green
export txtylw="\[\033[0;33m\]" # Yellow
export txtblu="\[\033[0;34m\]" # Blue
export txtpur="\[\033[0;35m\]" # Purple
export txtcyn="\[\033[0;36m\]" # Cyan
export txtwht="\[\033[0;37m\]" # White
export bldblk="\[\033[1;30m\]" # Black - Bold
export bldred="\[\033[1;31m\]" # Red
export bldgrn="\[\033[1;32m\]" # Green
export bldylw="\[\033[1;33m\]" # Yellow
export bldblu="\[\033[1;34m\]" # Blue
export bldpur="\[\033[1;35m\]" # Purple
export bldcyn="\[\033[1;36m\]" # Cyan
export bldwht="\[\033[1;37m\]" # White
export unkblk="\[\033[4;30m\]" # Black - Underline
export undred="\[\033[4;31m\]" # Red
export undgrn="\[\033[4;32m\]" # Green
export undylw="\[\033[4;33m\]" # Yellow
export undblu="\[\033[4;34m\]" # Blue
export undpur="\[\033[4;35m\]" # Purple
export undcyn="\[\033[4;36m\]" # Cyan
export undwht="\[\033[4;37m\]" # White
export bakblk="\[\033[40m\]"   # Black - Background
export bakred="\[\033[41m\]"   # Red
export badgrn="\[\033[42m\]"   # Green
export bakylw="\[\033[43m\]"   # Yellow
export bakblu="\[\033[44m\]"   # Blue
export bakpur="\[\033[45m\]"   # Purple
export bakcyn="\[\033[46m\]"   # Cyan
export bakwht="\[\033[47m\]"   # White
export txtrst="\[\033[0m\]"    # Text Reset

export PROMPT_DIRTRIM='2'

# GIT STATUS
# unstaged (*) and staged (+)
GIT_PS1_SHOWDIRTYSTATE=true
# If something is stashed, then a '$'
GIT_PS1_SHOWSTASHSTATE=false
#If there're untracked files, then a '%'
GIT_PS1_SHOWUNTRACKEDFILES=false
# "<" indicates you are behind, ">" indicates you are ahead, and "<>" indicates you have diverged.
GIT_PS1_SHOWUPSTREAM="auto"

if [ -f /usr/share/git/git-prompt.sh ]; then
	source /usr/share/git/git-prompt.sh
	if [[ $( type -t __git_ps1 ) == "function" ]]; then
		git_p="\$(__git_ps1 '${txtrst}${bldgrn}git:%s${txtrst}${txtrst}')"
	fi
fi

export PS1="${txtblu}\w${txtrst} ${git_p}\n${txtgrn}└─▶${txtrst} "



function command_not_found_handle () {
  >&2 echo "
  ┌──────────────────┐
  │ Unknown command! │
  └────. .───────────┘
        V
       ╭─╮
       ⌾ ⌾
       │▕│
       ╰─╯" 
  echo $0 $1
  return 127
}

export -f command_not_found_handle

case ${TERM} in
	xterm*|rxvt*|screen*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD}\007"'
	;;
esac

##
# functions
##
function psgrep() {
	ps aux | grep "$1" | grep -v "grep"
}

function whatthecommit(){
	curl -s http://whatthecommit.com/index.txt
}

function findhere() {
	find . -iname "*$@*"
}

function findin(){
	grep -liR $@
}

alias ls='ls --color=auto --group-directories-first'
alias ll='ls -lh'
alias cl="clear"
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'

[ -f /usr/bin/tmux ] && alias tmux='/usr/bin/tmux -u'
[ -f /usr/bin/colordiff ] && alias diff=/usr/bin/colordiff

if [ -f /usr/bin/xclip ]; then
	alias setclip="/usr/bin/xclip -selection c"
	alias getclip="/usr/bin/xclip -selection c -o"
fi

complete -o default -o nospace -F _git g

##
# Archlinux
##
if [ -f /etc/arch-release ]; then 
	[ -f /usr/bin/alsi ] && alias alsi="/usr/bin/alsi -l"
	[ -f /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion
fi

[ -f ~/.bashrc.local ] && . ~/.bashrc.local
